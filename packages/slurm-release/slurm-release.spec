Name: slurm-release
Version: 8
Release: 2%{?dist}
Summary: GSI Slurm Packages
License: LGPLv3
Source0: RPM-GPG-KEY-slurm
Source1: slurm.repo
BuildArch: noarch
#Requires: epel-release = 8

%description
This package contains the Slurm Packages for the GSI Virgo Clusters,
GPG key as well as configuration for yum/dnf. Those packages depend
on enabled EPEL and Powertools repos.

%prep

%build

%install
install -p -m 644 -D %{SOURCE0} \
  %{buildroot}%{_sysconfdir}/pki/rpm-gpg/RPM-GPG-KEY-slurm
install -p -m 644 -D -t %{buildroot}%{_sysconfdir}/yum.repos.d %{SOURCE1}

%files
%defattr(-,root,root,-)
%config(noreplace) %{_sysconfdir}/yum.repos.d/*
%{_sysconfdir}/pki/rpm-gpg/*

%changelog
* Tue Jan 16 2024 Dennis Klein <d.klein@gsi.de> - 8-2
- Fix debuginfo repo url
* Mon Jan 15 2024 Dennis Klein <d.klein@gsi.de> - 8-1
- Initial release for rocky+epel-8
